import {createItem} from './modules/create-new-item';
import {clearRows} from './modules/delete-row';
import {importData} from './modules/import-data';
import {exportData} from './modules/export-data';
import {demoRows} from './modules/demo-rows';
import {search} from './modules/search';
import {pagination} from './modules/pagination';
import {sort} from './modules/sort';
import {dragAndDrop} from './modules/drag-and-drop';
import {addNewRow} from './modules/add-new-row';

const app = {

  table: document.getElementById('items-list'),
  itemDOM: document.getElementById('items-list').children,

  hrefPage: 1,
  currentHref: window.location.href,

  filters: {
    search: document.getElementById('pFilter')
  },

  buttons: {
    add: document.getElementById('add-new-row-btn'),
    delete: document.getElementById('delete-row-btn'),
    demo: document.getElementById('demo-data-btn'),
    clear: document.getElementById('clear-table-btn'),
    import: document.getElementById('import-table-btn'),
    export: document.getElementById('export-table-btn'),
    sorts: document.getElementsByClassName('sort-arrow')
  },

  forms: {
    addRow: document.getElementById('add-new-row-form'),
    importTable: document.getElementById('import-table-form'),
    exportTable: document.getElementById('export-table-form')
  },

  content: {
    add: document.getElementById('add-row'),
    import: document.getElementById('import-table'),
    export: document.getElementById('export-table')
  },

  data: [],

  cellOptions: [],

  generateRandomCountOfItems: function() {
    this.randomItems = Math.floor(Math.random() * 10) + 1;
  },

  eventDragAndDrop: function(options) {
    for(let i = 0; i < this.itemDOM.length; i++) {
      this.itemDOM[i].onmousedown = function(e) {
        this.style.top = e.pageY - this.offsetHeight / 2 + 'px';        
        dragAndDrop.init(this, this.table, e, options);
      };
    };
    // dragAndDrop.init(this.table);
  },

  display: function(data, options) {
    this.table.innerHTML = '';
    pagination.init(data, app.table, this.hrefPage, this.currentHref);
    document.getElementsByClassName('table')[0].classList.remove('dragging-table');
    this.eventDragAndDrop(options);
  },

  start: function() {
    createItem.addNewItemToData(app.data, 'Some Name', '1', 'No');
  },

  animSlideShowElement(el) {
    let elements = document.getElementsByClassName('can-show-block');

    for(let i = 0; i < elements.length; i++) {
      elements[i].classList.remove('anim-show');
      elements[i].classList.add('anim-hide');
    }

    if(el.classList.contains('anim-hide')) {
      el.classList.remove('anim-hide');
      el.classList.add('anim-show');
    } else {
      el.classList.remove('anim-show');
      el.classList.add('anim-hide');
    }
  },

  setCellOptions: function() {
    for(let i = 0; i < this.itemDOM[0].children.length; i++) {
      this.cellOptions.push(this.itemDOM[0].children[i].offsetWidth + 'px');
    }
    this.cellOptions.push(this.itemDOM[0].offsetWidth + 'px');
  }
};
app.start();
app.setCellOptions();
app.display(app.data, app.cellOptions);


app.buttons.add.addEventListener('click', function() {
  app.animSlideShowElement(app.content.add);
});

app.forms.addRow.addEventListener('submit', function(event) {
  event.preventDefault();
  let item = addNewRow.init(this);

  createItem.addNewItemToData(app.data, item.name, item.qty, item.available);
  app.display(app.data, app.cellOptions);
});

app.buttons.demo.addEventListener('click', function() {
  app.generateRandomCountOfItems();
  demoRows.generateDemoRows(app.data, app.randomItems);
  app.display(app.data, app.cellOptions);
});

app.buttons.delete.addEventListener('click', function() {
  clearRows.setCheckedItems(app.table);
  clearRows.deleteRows(app.data);
  app.display(app.data, app.cellOptions);
});

app.buttons.clear.addEventListener('click', function() {
  app.data = [];
  app.display(app.data, app.cellOptions);
});

app.buttons.import.addEventListener('click', function() {
  app.animSlideShowElement(app.content.import);
});

app.buttons.export.addEventListener('click', function() {
  app.animSlideShowElement(app.content.export);
  exportData.insertTextToTextarea(app.data);
});

app.forms.importTable.addEventListener('submit', function(event){
  event.preventDefault();
  app.data = importData.getData(this, app.data);
  app.display(app.data, app.cellOptions);
});

app.forms.exportTable.addEventListener('submit', function(event){
  event.preventDefault();
  exportData.copyTextFromTextarea();
});

app.filters.search.addEventListener('keyup', function(event) {
  event.preventDefault();
  search.findDataElements(app.table, this.value);
});

window.addEventListener('hashchange', function(e){
  let regExp = /\w+$/;
  let href = window.location.href;
  app.hrefPage = href.match(regExp)[0];
  pagination.error(href.match(regExp)[0]);
  app.display(app.data, app.cellOptions);
});

for(let i = 0; i < app.buttons.sorts.length; i++) {
  app.buttons.sorts[i].addEventListener('click', function() {
    app.display(sort.init(this, app.data), app.cellOptions);
  });
};
