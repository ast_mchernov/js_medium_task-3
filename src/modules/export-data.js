export const exportData = {

  clearElementsID: function(data) {
    let tempObj = data;
    
    for(let i = 0; i < tempObj.length; i++) {
      delete tempObj[i].id;
    }

    return tempObj;
  },

  insertTextToTextarea(data) {
    let tempObj = JSON.stringify(this.clearElementsID(data));
    document.getElementById('pExport').value = tempObj;
    document.getElementById('export-table-copied').style.opacity = '0';
  },

  copyTextFromTextarea() {
    document.getElementById('pExport').select();
    document.execCommand('copy');
    document.getElementById('export-table-copied').style.opacity = '1';
  }

}