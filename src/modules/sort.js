export const sort = {
  sort: null,
  sortType: 'down',

  compareValues: function(key, order='asc') {
    return function(a, b) {
      if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        return 0;
      }
  
      const varA = (typeof a[key] === 'string') ?
        a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string') ?
        b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order == 'desc') ? (comparison * -1) : comparison
      );
    };
  },

  changeColorToDefault: function() {
    let items = document.getElementsByClassName('sort-name');

    for(let i = 0; i < items.length; i++) {
      items[i].style.color = 'inherit';
    }
  },

  init: function(element, data) {
    this.sort = element.dataset.sort;
    this.changeColorToDefault();
    element.parentNode.style.color = '#5cb85c';
    
    if(element.classList.contains('glyphicon-arrow-down')) {
      this.sortType = 'desc';
      element.classList.remove('glyphicon-arrow-down');
      element.classList.add('glyphicon-arrow-up');
    } else {
      this.sortType = 'asc';
      element.classList.remove('glyphicon-arrow-up');
      element.classList.add('glyphicon-arrow-down');
    }

    return data.sort(this.compareValues(this.sort, this.sortType));
  }
}
