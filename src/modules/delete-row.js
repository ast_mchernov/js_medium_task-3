export const clearRows = {
  checkedItemsId: [],

  setCheckedItems: function(table) {
    let items = table.getElementsByTagName('input');
    
    for(let i = 0; i < items.length; i++) {
      if(items[i].checked == true) {
        this.checkedItemsId.push(items[i].closest('tr').dataset.id);
      }
    }
  },

  clearItemsId: function() {
    this.checkedItemsId = []
  },

  deleteRows: function(data) {

    for(let i = 0; i < data.length; i++) {

      for(let j = 0; j < this.checkedItemsId.length; j++) {

        if(data[i].id === this.checkedItemsId[j]) {
          data.splice(i, 1);
        }
      }
    }
    this.clearItemsId();
  }
}