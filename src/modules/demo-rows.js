import {createItem} from './create-new-item';

export const demoRows = {

  randomName: '',
  possible: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
  randomSymbols: null,
  randomQty: null,
  randomAvailability: null,

  generateRandomSymbols: function() {
    this.randomSymbols = Math.floor(Math.random() * 7) + 3;
  },

  generateRandomName: function() {
    this.generateRandomSymbols();

    for (let i = 0; i < this.randomSymbols; i++) {
      this.randomName += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
    }
  },

  clearRandomName: function() {
    this.randomName = '';
  },

  generateRandomQty: function() {
    this.randomQty = Math.floor(Math.random() * 1000);
  },

  setRandomAvailability: function() { 
    if(Boolean(Math.round(Math.random()))) {
      this.randomAvailability = 'Yes'
    } else {
      this.randomAvailability = 'No'
    }
  },

  generateDemoRows: function(data, randomItems) {

    for(let i = 0; i < randomItems; i++) {
      this.generateRandomName();
      this.generateRandomQty();
      this.setRandomAvailability();

      createItem.addNewItemToData(data, this.randomName, this.randomQty, this.randomAvailability);
      
      this.clearRandomName();
    }
  }

}