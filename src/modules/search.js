export const search = {

  showAllDOMElement: function(dom) {

    for(let i = 0; i < dom.length; i++) {
      dom[i].style.display = 'table-row';
    }
  },

  selectTextByColor: function(text, original) {
    return original.replace(text, `<span class="color-orange">${text}</span>`);
  },

  ifEmptyDeleteSelected: function(dom) {
    for(let i = 0; i < dom.length; i++) {
      if(dom[i].getElementsByClassName('color-orange').length > 0) {
        dom[i].getElementsByClassName('color-orange')[0].style.color = '#000000';
      }
    }
  },
  
  findDataElements: function(dom, value) {
    let tableDOM = dom.getElementsByTagName('tr');

    if(value != '') {

      for(let i = 0; i < tableDOM.length; i++) {
        let name = tableDOM[i].children[1].textContent;

        if(name.indexOf(value) != -1) {

          tableDOM[i].style.display = 'table-row';
          tableDOM[i].children[1].innerHTML = this.selectTextByColor(value, name);

        } else {
          tableDOM[i].style.display = 'none';
        }
      }

    } else {
      this.showAllDOMElement(tableDOM);
      this.ifEmptyDeleteSelected(tableDOM);
    }

  }

}