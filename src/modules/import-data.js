import {serializeArray} from './serialize-form';

export const importData = {

  getData: function(form) {
    let newObj = serializeArray(form);
    let parsedObj = JSON.parse(newObj.pImport);

    for(let i = 0; i < parsedObj.length; i++) {
      parsedObj[i].id = Math.random().toString(36).substring(2);
    }
    return parsedObj;
  },
}