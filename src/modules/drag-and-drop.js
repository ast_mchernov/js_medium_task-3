// import tableDragger from 'table-dragger';

export const dragAndDrop = {

  changeItemsOrder: function(item1, item2) {
    let temp = item1.innerHTML;
    let temp2 = item2.innerHTML;
    
    item1.innerHTML = temp2;
    item2.innerHTML = temp;
  },

  compare: function(item, dom, position, event) {
    let tableRow = document.getElementsByTagName('tr');

    for(let i = 1; i < tableRow.length; i++) {
      if(item.getBoundingClientRect().top > tableRow[i].getBoundingClientRect().top) {
        item.style.order = i;
        tableRow[i].style.order = i - 1;
        tableRow[i+1].style.order = i + 1;

        if (item.getBoundingClientRect().top - tableRow[i].getBoundingClientRect().top >= tableRow[i].offsetHeight) {
          tableRow[i].style.marginBottom = '0px';
        } else {
          tableRow[i].style.marginBottom = item.getBoundingClientRect().top - tableRow[i].getBoundingClientRect().top + 'px';
        }

      }
    }    
  },

  changeItemWidth: function(item, dom, options) {
    let tableRow = document.getElementsByTagName('tr');

    for(let i = 0; i < tableRow.length; i++) {

      for(let j = 0; j < tableRow[i].children.length; j++) {
        tableRow[i].children[j].style.width = options[j];
      }
      tableRow[i].style.width = options[tableRow[i].children.length];
    }
  },


  init: function(item, dom, e, options) {
    item.style.position = 'absolute';
    item.style.zIndex = 1000;
    item.style.transitionDuration = '0s';
    item.classList.add('dragging');
    document.getElementsByClassName('table')[0].classList.add('dragging-table');
    let position = event.pageY - item.offsetHeight / 2;

    let compareFunc = this.compare.bind(this);
    let tableRow = document.getElementsByTagName('tr');

    document.onmousemove = function(event) {
      position = event.pageY - item.offsetHeight / 2;
      item.style.top = `${position}px`;
      compareFunc(item, dom, +position, event);
    }
    
    item.onmouseup = function() {
      document.onmousemove = null;
      item.onmouseup = null;
      item.style.position = 'relative';
      item.style.top = '0';
      item.style.zIndex = 1;

      for(let i = 0; i < tableRow.length; i++) {
        tableRow[i].style.marginBottom = '0px';
      }
    }

    this.changeItemWidth(item, dom, options);

    // tableDragger(document.getElementsByClassName('table')[0], { mode: "row", onlyBody: true, dragHandler: "tr" });
    // Or we can do drag & drop with this library
  }
}