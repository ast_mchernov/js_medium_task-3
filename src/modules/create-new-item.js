export const createItem = {

  randomId: null,
  elementDOM: null,
  elementData: [],

  generateRandomId: function() {
    this.randomId = Math.random().toString(36).substring(2);
  },

  createDOMItem: function(length, name, qty, availability, id) {
    this.elementDOM = `<tr data-id=${id}>  
                        <th class="item-id" scope="row">${length}</th>
                        <td class="item-name">${name}</td>
                        <td class="item-qty">${qty}</td>
                        <td>${availability}</td>
                        <td>
                          <div class="checkbox">
                            <label><input type="checkbox" name="pDelete" id="pDelete">Delete</label>
                          </div>
                        </td>
                      </tr>`;
    return this.elementDOM;
  },

  createDataItem: function(name, qty, availability) {
    this.elementData = {
                          id: this.randomId,
                          name: name,
                          qty: qty,
                          availability: availability,
                          delete: false
                        };
  },

  addNewItemToData: function(data, name, qty, availability) {
    this.generateRandomId();
    this.createDataItem(name, qty, availability);
    data.push(this.elementData);
  }

}