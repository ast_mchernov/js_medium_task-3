import {createItem} from './create-new-item';

export const pagination = {
  pagination: document.getElementsByClassName('main-pagination')[0],
  elementsLength: null,

  pageData: {},
  paginationDOM: null,
  pageSize: 10,

  checkElementsLength(data, currentHref) {

    if(data.length > 10) {
      this.initPagination();
      document.getElementById('main-pagination').innerHTML = this.paginationDOM;
      
      document.getElementsByClassName('page-link--input')[0].addEventListener('change', function(event) {
        event.preventDefault();
        
        let href = currentHref,
            index = href.indexOf('/#page-'),
            cut = href.length - index;

        window.location.href = window.location.href.slice(0, -(cut)) + '#page-' + this.value;
      });
    } else {
      document.getElementById('main-pagination').innerHTML = '';
    }
  },

  paginator: function(items, page, per_page) {
    var page = page || 1,
        per_page = per_page || 10,
        offset = (page - 1) * per_page,
    
        paginatedItems = items.slice(offset).slice(0, per_page),
        total_pages = Math.ceil(items.length / per_page);
    
    this.pageData = {
      page: page,
      per_page: per_page,
      pre_page: page - 1 ? page - 1 : null,
      next_page: (total_pages > page) ? +page + 1 : null,
      total: items.length,
      total_pages: total_pages,
      data: paginatedItems
    };
  },

  initPagination: function() {
    this.paginationDOM = `<ul class="pagination justify-content-center">
                            <li class="page-item ${this.pageData.pre_page == null ? 'disabled' : null}">
                                <a class="page-link  page-link--prev" href="#page-${this.pageData.pre_page == null ? this.pageData.page : this.pageData.pre_page}" tabindex="-1">Previous</a>
                            </li>

                            <li class="page-item  ${this.pageData.page === 1 ? 'page-item--hide' : 'page-item--first'}">
                                <a class="page-link  page-link--first" href="#page-1">1</a>
                            </li>

                            <li class="page-item  page-item--input">
                                <input type="number" class="page-link  page-link--input" value="" placeholder="...">
                            </li>
                            
                            <li class="page-item  page-item--last">
                                <a class="page-link  page-link--last" href="#page-${this.pageData.total_pages}">${this.pageData.total_pages}</a>
                            </li>

                            <li class="page-item  ${this.pageData.next_page == null ? 'disabled' : null}">
                                <a class="page-link  page-link--next" href="#page-${this.pageData.next_page == null ? this.pageData.page : this.pageData.next_page}">Next</a>
                            </li>
                          </ul>
                          <div class="page-item  page-item--curent">
                            <a class="page-link  page-link--current  page-link--active" href="#page-${this.pageData.page}">${this.pageData.page}</a>
                          </div> `;
  },

  returnCurrentPage: function(data, number) {
    this.paginator(data, number, this.pageSize);
    return this.pageData;
  },

  error: function(href) {
    if(href > this.pageData.total_pages) {
      document.getElementById('page-error').style.display = 'block';
    } else {
      document.getElementById('page-error').style.display = 'none';
    }
  },

  init: function(data, dom, page, href) {
    this.paginator(data, page, this.pageSize);
    this.checkElementsLength(data, href);

    for(let i = 0; i < this.pageData.data.length; i++) {
      dom.innerHTML += createItem.createDOMItem((page - 1)* this.pageSize + i + 1, this.pageData.data[i].name, this.pageData.data[i].qty, this.pageData.data[i].availability, this.pageData.data[i].id);
    };

  }
}