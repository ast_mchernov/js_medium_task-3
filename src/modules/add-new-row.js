import {serializeArray} from './serialize-form';

export const addNewRow = {

   validateNumbers: function(data) {
    if(data.match(/^-{0,1}\d+$/)) {
      return data;
    } else if(data.match(/^\d+\.\d+$/)){
      return data;
    } else {
      return false;
    }
  },

  init: function(data) {
    let form = serializeArray(data);

    let available = form.pAvailability == 'on' ? 'Yes' : 'No';
    let qty = null;

    if(this.validateNumbers(form.pQty) == false) {
      document.getElementById('qty-error').style.display = "block";
      return false;
    } else {
      qty = form.pQty;
      document.getElementById('qty-error').style.display = "none";
    }

    // createItem(data.pTitle, data.pSKU, data.pPrice, data.pDesc, available);
    return {
      name: form.pName,
      qty: qty,
      available: available
    };
  }
}